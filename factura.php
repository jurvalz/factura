<?php
header("Content-type: application/json; charset=utf-8");
include('cantidad_letras.php');
# empresa, contribuyente
$emisor = array(
    'tipodoc' => '6', # cod tipo de documento
    'ruc' => '20123456789',
    'razon_social' => 'Inleggo SAC',
    'nombre_comercial' => 'Inleggo',
    'direccion' => 'LIMA',
    'ubigeo' => '150101',
    'departamento' => 'LIMA',
    'provincia' => 'LIMA',
    'distrito' => 'LIMA',
    'pais' => 'PE',
    'usuario_secundario' => 'MODDATOS', # user de prueba de sunat uso WS
    'clave_usuario_secundario' => 'MODDATOS',
);

$cliente = array(
    'tipodoc' => '6', # cod tipo de documento
    'ruc' => '1043431568',
    'razon_social' => 'Joseph Urbina',
    'direccion' => 'LIMA',
    'pais' => 'PE',
);

$comprobante = array(
    'tipodoc' => '01', # co01: factura, 03: boleta, 07: nc, 08: nd
    'serie' => 'F001', # F: factura, B: boleta
    'correlativo' => '1',
    'fecha_emision' => date('Y-m-d'),
    'moneda' => 'PEN', # PEN: soles, USD: solares
    'total_opgravadas' => 0,
    'total_opexoneradas' => 0,
    'total_opinafectas' => 0,
    'igv' => 0,
    'total' => 0,
    'total_texto' => '',
    'forma_pago' => 'Contado', # Credito, Contado
    'monto_pendiente' => 100
);

$cuotas = array(
    array(
        'cuota' => 'Cuota001',
        'monto' => 50,
        'fecha' => '2022-01-15'
    ),
    array(
        'cuota' => 'Cuota002',
        'monto' => 50,
        'fecha' => '2022-01-15'
    )
);

$detalle = array(
    array(
        'item' => 1,
        'codigo' => 'DHQ132',
        'descripcion' => 'ACEITE',
        'cantidad' => 2,
        'valor_unitario' => 50, # sin IGV
        'precio_unitario' => 59, # con IGV
        'tipo_precio' => '01',
        'igv' => 18,
        'porcentaje_igv' => 18,
        'valor_total' => 118,
        'unidad' => 'NIU',
        'codigo_afectacion_alt' => '10', # 10: gravado, 20: exonerado, 30: inafecto
        'codigo_afectacion' => '1000',
        'nombre_afectacion' => 'IGV',
        'tipo_afectacion' => 'VAT',
    ),
    array(
        'item' => 2,
        'codigo' => 'DHQ456',
        'descripcion' => 'LIBRO DEVOPS',
        'cantidad' => 1,
        'valor_unitario' => 30, # sin IGV
        'precio_unitario' => 30, # con IGV
        'tipo_precio' => '01',
        'igv' => 0,
        'porcentaje_igv' => 0,
        'valor_total' => 30,
        'unidad' => 'NIU',
        'codigo_afectacion_alt' => '20', # 10: gravado, 20: exonerado, 30: inafecto
        'codigo_afectacion' => '9997',
        'nombre_afectacion' => 'EXO',
        'tipo_afectacion' => 'VAT',
    ),
    array(
        'item' => 3,
        'codigo' => 'DHQ854',
        'descripcion' => 'MAMEY',
        'cantidad' => 1,
        'valor_unitario' => 10, # sin IGV
        'precio_unitario' => 10, # con IGV
        'tipo_precio' => '01',
        'igv' => 0,
        'porcentaje_igv' => 0,
        'valor_total' => 10,
        'unidad' => 'NIU',
        'codigo_afectacion_alt' => '30', # 10: gravado, 20: exonerado, 30: inafecto
        'codigo_afectacion' => '9998',
        'nombre_afectacion' => 'INA',
        'tipo_afectacion' => 'FRE',
    ),
);

# inicializar totales

$total_opgravadas = 0;
$total_opexoneradas = 0;
$total_opinafectas = 0;
$igv = 0;
$total = 0;

foreach ($detalle as $key => $value) {
    if($value['codigo_afectacion_alt']=='10'){
        $total_opgravadas = $total_opgravadas + $value['valor_total'];
    }
    if($value['codigo_afectacion_alt']=='20'){
        $total_opexoneradas = $total_opexoneradas + $value['valor_total'];
    }
    if($value['codigo_afectacion_alt']=='30'){
        $total_opinafectas = $total_opinafectas + $value['valor_total'];
    }
    $igv = $igv + $value['igv'];
    $total = $total + $value['valor_total'];
}
$letras = new EnLetras;
$comprobante['total_opgravadas']= $total_opgravadas;
$comprobante['total_opexoneradas']= $total_opexoneradas;
$comprobante['total_opinafectas']= $total_opinafectas;
$comprobante['igv']= $igv;
$comprobante['total']= $total;
$comprobante['total_texto']= $letras->ValorEnLetras($total,'Soles');

$json = json_encode($cliente, JSON_PRETTY_PRINT);
print_r($json);